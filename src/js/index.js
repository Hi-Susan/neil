import tippy, {followCursor} from 'tippy.js';
import Swiper from 'swiper';
import ScrollSpy from 'bootstrap/js/dist/scrollspy'
import emailjs from '@emailjs/browser';

import '../css/style.css';

const countryDisplay = () => {
  document.querySelectorAll
  const $countryButton = document.querySelectorAll('.country-item')
  
  const countrySelect = {
    remind() {
      this.timeoutID = undefined;
    },
    setup ($countryButton) { 
      const $countryList = $countryButton.nextSibling.nextElementSibling
      const $closeButton = $countryList.querySelector('.close-btn')
      
      $closeButton.addEventListener("click", () => {
        $countryList.classList.remove('show')
        this.timeoutID = setTimeout(()=>{$countryList.dataset.state = false}, 310);
      })
      
      if (typeof this.timeoutID === "number") {
        this.cancel();
      }
      if($countryList.dataset.state === 'true') {
        $countryList.classList.remove('show')
        this.timeoutID = setTimeout(()=>{$countryList.dataset.state = false}, 310);
      } else {
        $countryList.dataset.state = true
        this.timeoutID = setTimeout(() => {
          $countryList.classList.add('show');
        }, 100);
      }
    },
    cancel() {
      clearTimeout(this.timeoutID);
    },
  }
  
  $countryButton.forEach(function($ele) {
    if ($ele) $ele.addEventListener("click", () => countrySelect.setup($ele))
  });
} 
const anchorListener = () => {
  document.querySelectorAll('.list-group-item').forEach(($ele) => {
    $ele.addEventListener('click', (e) => { 
      e.target.classList.add('active')
    });
  });
}
const setMail = () => {
  const canvas = document.getElementById("canvas");
  const cxt = canvas.getContext("2d"); //canvas getContext 渲染環境
  let validate = "";
  const baseColor = ["#0000ff", "#CE0000", "#7E3D76", "#46A3FF"]; //底色
  const sColor = ["#B22222", "#F9F900", "#82D900", "#FFAF60"]; //干擾點顏色
  const fColor = ["#FFA07A", "#00BB00", "#EAC100", "#F75000"]; //文字顏色
  let indexColor = ""; //顏色組序號
  
  /*生成隨機顏色組合序號*/
  function randColor() {
    indexColor = "";
    indexColor = Math.floor(Math.random() * baseColor.length); //亂數取得 0~顏色陣列長度
    return indexColor;
  }
  
  /*生成6位隨機數*/
  function rand() {
    validate = "";
    // 大小寫英文 數字 排除 I l o O 0  ,並重複數字 增加出現機率
    const str =
      "123456789abcdefghijkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ123456789";
    const arr = str.split("");
    let ranNum;
    for (var i = 0; i < 6; i++) {
      ranNum = Math.floor(Math.random() * 66); //隨機數在[0,65]之間
      validate += arr[ranNum];
    }
    return validate;
  }
  
  /*干擾線的隨機x坐標值*/
  function lineX() {
    const ranLineX = Math.floor(Math.random() * 150);
    return ranLineX;
  }
  
  /*干擾線的隨機y坐標值*/
  function lineY() {
    const ranLineY = Math.floor(Math.random() * 40);
    return ranLineY;
  }
  
  /*更換內容*/
  function clickChange() {
    const i = randColor(); //執行randColor()取得顏色組序號
    //alert(i);
    cxt.beginPath();
    cxt.fillStyle = baseColor[i]; //底色
    cxt.fillRect(0, 0, 150, 40); //(x,y,width,height)
  
    /*生成干擾點 40*/
    for (let j = 0; j < 40; j++) {
      //產生一個新路徑，產生後再使用繪圖指令來設定路徑
      //若省略beginPath，則每點擊一次驗證碼會累積干擾線的條數
      cxt.beginPath();
      cxt.fillStyle = sColor[i]; //顏色
      const arcSize = (Math.floor(Math.random() * (50 - 5 + 1)) + 5) / 10; //亂數 取得介於0.5~5之間的值
      cxt.arc(lineX(), lineY(), arcSize, 0, 2 * Math.PI); //arc()產生一個圓形  context.arc(x,y,r,sAngle,eAngle,counterclockwise);
      cxt.fill();
    }
    cxt.fillStyle = fColor[i]; //隨機文字顏色
    cxt.font = "bold 25px Verdana";
    cxt.fillText(rand(), 10, 30); //把rand()生成的隨機數文本填充到canvas中
  }
  
  /*點擊驗證碼更換*/
  canvas.onclick = function (e) {
    e.preventDefault(); //阻止滑鼠點擊發生預設的行為
    clickChange();
  };
  
  /*輸入驗證碼與核對*/
  const contactForm = document.getElementById("contactForm");
  contactForm.addEventListener("submit", function(e) {
    e.preventDefault();
    const vad = contactForm.vad.value;
    document.getElementById("vad-group").classList.remove('error');
    //將字串利用toUpperCase()將小寫英文轉成大寫英文 進行比較
    if(vad.toUpperCase() === validate.toUpperCase()) {
      emailjs.init("user_ctbet2jOSlQhokTESgk0F");
      emailjs
        .sendForm(
          'service_hhyxlzs',
          'template_xdyiqne',
          '#contactForm',
          'SkoYQRcK5Q2F5ksM6'
        )
        .then(
          function(response) {
            document.getElementById("submit-success").classList.remove('hidden');
            setTimeout(() => {
              document.getElementById("submit-success").classList.add('hidden');
            }, 30000);
            console.log('SUCCESS!', response.status, response.text)
            contactForm.name.value = "";
            contactForm.phone.value = "";
            contactForm.email.value = "";
            contactForm.content.value = "";
          },
          function(error) {
            document.getElementById("submit-error").classList.remove('hidden');
            setTimeout(() => {
              document.getElementById("submit-error").classList.add('hidden');
            }, 30000);
            console.log('FAILED...', error)
          }
        )
      contactForm.vad.value = "";
      clickChange();
    } else {
      document.getElementById("vad-group").classList.add('error');
      setTimeout(() => {
        document.getElementById("vad-group").classList.remove('error');
      }, 30000);
      contactForm.vad.value = "";
      clickChange();
    }
    e.preventDefault();
  }); //改用placeholder設定預設內容
  
  
  clickChange();
}


window.onload = function() {
  countryDisplay();
  anchorListener();
  setMail()
};